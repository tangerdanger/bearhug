import sugar, uri, json, options, times, httpcore

type
  Image* = ref object of RootObj
    src: Uri

  TagType* = ref object
    name: string

  Tag* = ref object of RootObj
    name: string
    `type`: TagType

  TagSeq* = seq[Tag]

  BearList* = ref object
    bears*: seq[Bear]
    moreCount*: int

  Bear* = object
    id: int
    username: string
    bio: string
    age: int
    #tags: Option[TagSeq]
    #img: Option[Image]

when defined(js):
  include karax/prelude
  import karax/kajax, karax/kdom, karax/vdom

  type
    State = ref object
      bearList: Option[BearList]
      isSearchLoading: bool
      isBearsLoading: bool
      lastUpdate: Time

  func initState(): State =
    result.bearList = none[BearList]()
    result.isSearchLoading = false
    result.isBearsLoading = false
    result.lastUpdate = getTime()

  var state = initState()

  proc searchKeyDownHandler(e: Event, v: VNode) =
    echo "Searching for " & v.value

  proc buildHeader*(): VNode = 
    result = buildHtml(tdiv()):
      header(id = "mainNavBar"):
        tdiv(class = "navbar"):
          section(class = "navbar-section"):
            a(href = "/"):
              img(src = "/images/logo.png", id = "img-logo")
          section(class = "navbar search"):
            tdiv(class = "input-group input-inline"):
              input(class = "search", placeholder = "Search for anything", `type` = "text", id = "search-box", onKeyDown = searchKeyDownHandler)
            if state.isSearchLoading == true:
              tdiv(class = "loading"):
                text "Loading..."

  proc buildFooter*(): VNode = 
    result = buildHtml(tdiv()):
      footer(id = "mainFooter"):
        tdiv(class = "support-bar"):
          a(href = "/contact")
          a(href = "/chat")

  proc onBearClick(bear: Bear): proc() =
    result = proc() = 
      echo "Bear found"

  proc main() =
    proc updateBearList(httpStatus: int, response: kstring) =
      state.isBearsLoading = false
      if httpStatus.HttpCode != Http200:
        return

      let data = parseJson($response)
      let list = some(to(data, BearList))
      if list.isSome:
        echo "LISTED"
      #for b in data["bears"].items:
      #  let bear = Bear(
      #    id: b["id"].getInt,
      #    username: b["username"].getStr,
      #    age: b["age"].getInt,
      #  )
      #  if isSome[TagSeq](b["tags"]):
      #    bear.tags = some(b["tags"]).getElems
      #  if isSome(b["img"]):
      #    img: some(b["img"]).getFields()

        state.bearList = list
        state.lastUpdate = getTime()

    if state.bearList.isNone:
      state.isBearsLoading = true
      ajaxGet("http://127.0.0.1:8080/honeypot/10/0", @[], updateBearList)

    proc createDom(data: RouterData): VNode = 
      result = buildHtml(tdiv()):
        buildHeader()
        main():
          summary(class = "bear-summary"):
            p:
              text "These local bears are after your honey"
          if state.bearList.isSome:
            ul(class = "bear-list"):
              for bear in state.bearList.get().items:
                li(class = "bear"):
                  a(class = "bear-profile", onclick = onBearClick(bear))
  #                if isSome(bear.img):
  #                  img(src = $bear.img.src)
          if state.isBearsLoading:
            tdiv(class = "bear-loading"):
              p:
                text "Loading..."
        buildFooter()

    setRenderer createDom

when isMainModule:
  main()
