import jester, json, tables, os, asyncdispatch, options

var khtml: string

from client import Bear, TagSeq, Image, BearList

routes:

  get "/honeypot/@count/@index":
    var
      start = getInt(@"index", 0)
      count = getInt(@"count", 10)

    #TODO: Actual db stuff
    let moreCount = 10

    var list = BearList(bears: @[], moreCount: count)
    let only_bear = Bear(id: -1, username: "Ash", bio: "Test Bio", age: 69)
    list.add(only_bear)

    resp $(%response), "application/json"

  get "/":
    khtml = readFile(getStaticDir(request) / "bearhug.html")
    resp khtml

