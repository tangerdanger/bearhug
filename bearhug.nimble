# Package

version       = "0.1.0"
author        = "Ryan Cotter"
description   = "An app for bearded men that like cuddles"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["bearhug"]


# Dependencies

requires "nim >= 0.19.4"

task runServer, "Start the server":
  exec "mkdir -p bin"
  exec "nim c -r src/bearhugpkg/server.nim"

task runClient, "Start the client":
  exec "mkdir -p bin"
  exec "mkdir -p public"
  exec "cp -f static/bearhug.html public/bearhug.html"
  exec "nim js --nimcache:public --out:public/client.js src/bearhugpkg/client.nim"

